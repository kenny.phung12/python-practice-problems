# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    if word == word[::-1]:
        return True
    else:
        return False


# Self-assigned task (test function)
# Example 1: Word IS NOT a palindrome (False)
test_word = is_palindrome("today")
print(test_word)

# Example 1: Word IS a palindrome (True)
test_word = is_palindrome("civic")
print(test_word)

# Completed 03/24 - KP
