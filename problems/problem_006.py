# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    if age >= 18 or has_consent_form:
        return True
    else:
        return False


# Self-assigned task(s) (test function)
# Example 1: Person is UNDER 18 and NO consent form : output should = False
blah = can_skydive(16, None)
print(blah)

# Example 2: Person is UNDER 18 and HAS consent form : output should = False
blah2 = can_skydive(16, True)
print(blah2)

# Example 3: Person is OVER 18 and NO consent form : output should = False
blah3 = can_skydive(88, None)
print(blah3)

# Completed 03/24 - KP
