# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by
#   by both 3 and 5
# * The word "fizz" if number is evenly divisible by only
#   3
# * The word "buzz" if number is evenly divisible by only
#   5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def fizzbuzz(number):
    if number % 3 == 0 and number % 5 == 0:
        return "fizzbuzz"
    elif number % 5 == 0:
        return "buzz"
    elif number % 3 == 0:
        return "fizz"
    else:
        return number


# Self-assigned task (test function)
# Test 1: Number IS divisible by 3 AND 5, output = fizzbuzz
example_number = fizzbuzz(30)
print(example_number)

# Test 2: Number IS divisible by ONLY 3, output = fizz
example_number = fizzbuzz(33)
print(example_number)

# Test 3: Number IS divisible by ONLY 5, output = buzz
example_number = fizzbuzz(35)
print(example_number)

# Test 4: Number IS NOT divisible by 3 OR 5, output = number
example_number = fizzbuzz(31)
print(example_number)
# Completed 03/24 - KP
