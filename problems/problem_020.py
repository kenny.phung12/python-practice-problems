# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    if len(attendees_list) >= (len(members_list))/2:
        return print("#attendees >= #members")
    else:
        return print("#ofattendees < #ofmembers")


# Self-assigned task(s) (test function)
# Test 1: output = #attendees >= #members
has_quorum(4, 1)
