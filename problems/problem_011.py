# Complete the is_divisible_by_5 function to return the
# word "buzz" if the value in the number parameter is
# divisible by 5. Otherwise, just return the number.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_divisible_by_5(number):
    if number % 5 == 0:
        print("buzz")
    else:
        print(number)


# Self-assigned task (test function)
# Test 1 : Input IS divisible by 5, output = buzz
is_divisible_by_5(15)

# Test 2 : Input IS NOT divisible by 5, output = number
is_divisible_by_5(157)
# Completed 03/24 - KP
