# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    if (
        x > 0 and x < 10 and y > 0 and y < 10
    ):
        return True
    else:
        return False


# Self-assigned task(s) (test function)
# Test 1: x AND y IS inbound, output = True
test_coordinates = is_inside_bounds(1, 9)
print(test_coordinates)

# Test 2: ONLY x IS inbound, output = False
test_coordinates = is_inside_bounds(1, 99)
print(test_coordinates)

# Test 3: ONLY y IS inbound, output = False
test_coordinates = is_inside_bounds(-11, 9)
print(test_coordinates)

# Test 2: x AND y IS NOT inbound, output = False
test_coordinates = is_inside_bounds(11, 99)
print(test_coordinates)
