# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    avg_grade = sum(values)/len(values)
    if avg_grade >= 90:
        print("A")
    elif avg_grade >= 80 and avg_grade <= 90:
        print("B")
    elif avg_grade >= 70 and avg_grade <= 80:
        print("C")
    elif avg_grade >= 60 and avg_grade <= 70:
        print("D")
    else:
        print("F")


calculate_grade([50, 50, 100])
