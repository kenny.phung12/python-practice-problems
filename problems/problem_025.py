# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    if values == 0:
        return None
    total = sum(values)
    print(total)


calculate_sum([1, 2, 3, 4, 5])
