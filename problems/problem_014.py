# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    if (
        "flour" in ingredients
        and "eggs" in ingredients
        and "oil" in ingredients
    ):
        return True
    else:
        return False


# Self-assigned task (test function)
# Test 1: ALL ingredients are present, output = True
example_list_of_ingredients = can_make_pasta(("flour", "eggs", "oil"))
print(example_list_of_ingredients)

# Test 2: SOME ingredients are present, output = False
example_list_of_ingredients = can_make_pasta(("rice", "eggs", "oil"))
print(example_list_of_ingredients)

# Test 3: All ingredients are present (list is >3 items), output = True
example_list_of_ingredients = can_make_pasta(("flour", "eggs", "oil", "beef"))
print(example_list_of_ingredients)

# Test 4: SOME ingredients are present (list is >3 items), output = False
example_list_of_ingredients = can_make_pasta(("rice", "eggs", "oil", "sugar"))
print(example_list_of_ingredients)
# Completed 03/24 - KP
