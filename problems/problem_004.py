# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    if value1 >= value2 and value1 >= value3:
        return value1
    elif value2 >= value1 and value2 >= value3:
        return value2
    else:
        return value3


#  Self-assigned task(s) (test function)
# Test 1: value1 = highest value
example_values = max_of_three(125512215, 5125, 1124)
print(example_values)

# Test 2: value2 = highest value
example_values = max_of_three(1215, 995125125, 1124)
print(example_values)

# Test 3: value3 = highest value
example_values = max_of_three(5125, 95125, 995125)
print(example_values)

# Test 4: two values = highest value
example_values = max_of_three(5125, 125, 5125)
print(example_values)

# Test 5: all values = highest value
example_values = max_of_three(5125, 5125, 5125)
print(example_values)

# Completed 03/24 - KP
