# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
    if value1 == value2:
        return value1, value2
    elif value1 < value2:
        return value1
    else:
        return value2


#  Self-assigned task(s) (test function)
# Test 1: "Values are the same; return either"
example_value = minimum_value(100, 100.0)
print(example_value)

# Test 2: "Print the minimum out of two values"
example_value = minimum_value(100, 900)
print(example_value)

# Completed 03/24 - KP
